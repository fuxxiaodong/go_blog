package routers

import (
	"blog/controllers"
	"github.com/astaxie/beego"
)

func init() {
	beego.Router("/", &controllers.MainController{}, "*:Get")
	beego.Router("/areas", &controllers.AreaController{}, "*:Get")

}
