package controllers

import (
	"blog/models"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	//"gopkg.in/gin-gonic/gin.v1/json"
)

type AreaController struct {
	beego.Controller
}

type Area struct {
	error int
	errmsg string
}

func (this*AreaController)RetData(resp map[string]interface{})  {
	this.Data["json"] =resp
	this.ServeJSON()
}

func (c *AreaController) Get() {


	resp :=make(map[string]interface{})

	defer c.RetData(resp)
	var areas []models.Area

	o :=orm.NewOrm()
	num,err :=o.QueryTable("area").All(&areas) //读取资源句柄
	if err !=nil {
		beego.Error("数据格式错误")
		resp["errno"] = 4001
		resp["errmsg"] = "查询失败"
        c.RetData(resp)
		return
	}
	if num ==0{
		beego.Error("数据格式错误")
		resp["errno"] = 4002
		resp["errmsg"] = "查询失败"
		c.RetData(resp)
		return
	}
	resp["errno"] = 200
	resp["errmsg"] = "ok"
	resp["data"] = &areas
	beego.Info(resp)
	c.RetData(resp)



}
