package models

import (
	"github.com/astaxie/beego/orm"
	_ "github.com/go-sql-driver/mysql"
	"time"
)

type User struct {
	Id                 int            `json:"user_id"`
	Name               string         `orm:"size(32);unique" json:"name"`
	Password_hash      string          `orm:"size(128);" json:"password"`
	Mobile             string           `orm:"size(11);unique" json:"mobile"`
	Real_name          string           `orm:"size(20)" json:"real_name"`
	Id_card            string           `orm:"size(256)" json:"id_card"`
	Avatar_url         string           `orm:"size(256)" json:"avatar_url"`
	Houses             []*House           `orm:"reverse(many)" json:"houses"`
	Orders             []*OrderHouse   `orm:"reverse(many)" json:"orders"`
}
type House struct {
	Id                 int            `json:"house_id"`
	User               *User         `orm:"rel(fk)" json:"user_id"`
	Area               *Area          `orm:"rel(fk)" json:"area_id"`
	Title             string           `orm:"size(64)" json:"title"`
	Price              int             `orm:"default(0)" json:"price"`
	Address            string           `orm:"size(512)" orm:"default("")" json:"address"`
	Room_count         int              `orm:"default(1)" json:"room_count"`
	Acreage             int             `orm:"default(0)" json:"acreage"`
	Unit            string               `orm:"size(32)" orm:"default("")" json:"unit"`
	Capacity        int                   `orm:"default(1)" json:"capacity"`
	Beds            string                `orm:"size(64)" orm:"default("")" json:"beds"`
	Deposit         int                   `orm:"default(0)" json:"deposit"`
	Min_days        int                   `orm:"default(1)" json:"min_days"`
	Max_days        int                   `orm:"default(0)" json:"min_days"`
	Order_count      int                   `orm:"default(0)" json:"order_count"`
	Index_image_url string                 `orm:"size(256)" orm:"default("")" json:"index_image_url"`
	Facilities       []*Facility          `orm:"reverse(many)" json:"facilities"`
	Images       []*HouseImage          `orm:"reverse(many)" json:"img_urls"`
	Orders       []*OrderHouse          `orm:"reverse(many)" json:"orders"`
	Ctime        time.Time          `orm:"auto_now_add;type(datetime)" json:"ctime"`
}
type Facility struct {
	Id                 int            `json:"f_id"`
	Name               string         `orm:"size(32)`
	Houses             []*House           `orm:"rel(m2m)"`
}
type HouseImage struct {
	Id                 int            `json:"house_image_id"`
	Url               string         `orm:"size(256)" json:"url"`
	Houses             *House           `orm:"rel(fk)" json:"houses_id"`
}
type Area struct {
	Id                 int            `json:"aid"`
	Name               string         `orm:"size(32);unique" json:"aname"`
	Houses             []*House           `orm:"reverse(many)" json:"houses"`
}
type OrderHouse struct {
	Id                 int            `json:"order_id"`
	User               *User         `orm:"rel(fk)" json:"user_id"`
	Houses             *House           `orm:"rel(fk)" json:"houses_id"`
	Begin_date         time.Time        `orm:"type(datetime)"`
	End_date           time.Time        `orm:"type(datetime)"`
	Days               int
	House_price        int
	Amount             int
	Status             string          `orm:"default(Wait_ACCEPT)"`
	Comment           string           `orm:"size(512)"`
	Ctime             time.Time        `orm:"auto_now_add;type(datetime)" json:"ctime"`

}

// init 初始化
func init() {
	orm.RegisterDataBase("default", "mysql", "root:root@/go?charset=utf8")
	orm.RegisterModel(new(User),new(Area),new(House),new(HouseImage),new(Facility),new(OrderHouse) )

}


