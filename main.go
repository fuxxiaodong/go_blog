package main

import (
	_ "blog/models"
	_ "blog/routers"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"

)

func main() {
	orm.Debug = true

	//自动建表
	orm.RunSyncdb("default", false, false)
	beego.Run()
}


